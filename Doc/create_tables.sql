# Dump of table cash_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS cash_log;

CREATE TABLE cash_log (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  open_id varchar(50) DEFAULT NULL,
  phone varchar(30) DEFAULT NULL,
  we_chat varchar(30) DEFAULT NULL,
  pay decimal(8,2) DEFAULT NULL,
  create_time timestamp NULL DEFAULT NULL,
  status varchar(30) DEFAULT NULL,
  result varchar(30) DEFAULT NULL,
  flag varchar(2) DEFAULT NULL,
  message varchar(256) DEFAULT NULL,
  o_time timestamp NULL DEFAULT NULL,
  attr1 varchar(128) DEFAULT NULL,
  attr2 varchar(128) DEFAULT NULL,
  attr0 varchar(128) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table dic
# ------------------------------------------------------------

DROP TABLE IF EXISTS dic;

CREATE TABLE dic (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  type varchar(30) NOT NULL DEFAULT '' COMMENT '字典类型',
  code varchar(30) NOT NULL DEFAULT '' COMMENT '值',
  name varchar(30) NOT NULL DEFAULT '',
  remark varchar(128) DEFAULT '',
  flag varchar(2) DEFAULT '1',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table goods
# ------------------------------------------------------------

DROP TABLE IF EXISTS goods;

CREATE TABLE goods (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  type varchar(10) DEFAULT NULL,
  name varchar(10) DEFAULT NULL,
  state varchar(20) DEFAULT NULL,
  buy decimal(8,2) DEFAULT NULL,
  sell decimal(8,2) DEFAULT NULL,
  more decimal(8,2) DEFAULT NULL,
  bull decimal(8,2) DEFAULT NULL,
  status varchar(20) DEFAULT NULL,
  time timestamp NULL DEFAULT NULL,
  create_time timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS menu;

CREATE TABLE menu (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  code varchar(30) NOT NULL DEFAULT '',
  name varchar(30) NOT NULL DEFAULT '',
  icon varchar(50) DEFAULT '',
  icon2 varchar(50) DEFAULT '',
  url varchar(128) NOT NULL DEFAULT '',
  p_id int(11) NOT NULL DEFAULT '0',
  target varchar(30) DEFAULT NULL,
  flag varchar(2) NOT NULL DEFAULT '1',
  bgcolor varchar(50) DEFAULT '',
  display int(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS role;

CREATE TABLE role (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  code varchar(30) NOT NULL DEFAULT '',
  name varchar(30) NOT NULL DEFAULT '',
  remark VARCHAR(30) DEFAULT NULL,
  flag varchar(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table role_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS role_menu;

CREATE TABLE role_menu (
  role_id int(11),
  menu_id int(11),
  PRIMARY KEY (role_id,menu_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS user_role;

CREATE TABLE user_role (
  role_id int(11),
  user_id int(11),
  PRIMARY KEY (role_id,user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table pay_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS pay_log;

CREATE TABLE pay_log (
  id bigint(11) NOT NULL AUTO_INCREMENT,
  open_id varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  create_time timestamp NULL DEFAULT NULL,
  pay decimal(8,2) DEFAULT NULL,
  status varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  pay_no varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  attr1 varchar(128) DEFAULT NULL,
  attr2 varchar(128) DEFAULT NULL,
  attr0 varchar(128) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table pay_stream
# ------------------------------------------------------------

DROP TABLE IF EXISTS pay_stream;

CREATE TABLE pay_stream (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  open_id varchar(50) DEFAULT NULL,
  code varchar(30) DEFAULT NULL,#类型值，方便后期统计
  type varchar(30) DEFAULT NULL,
  pay decimal(8,2) DEFAULT NULL,
  before_pay decimal(8,2) DEFAULT NULL,
  after_pay decimal(8,2) DEFAULT NULL,
  create_time timestamp NULL DEFAULT NULL,
  order_no varchar(128) DEFAULT NULL,
  attr0 varchar(128) DEFAULT NULL,
  attr1 varchar(128) DEFAULT NULL,
  attr2 varchar(128) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table trade
# ------------------------------------------------------------

DROP TABLE IF EXISTS trade;

CREATE TABLE trade (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  open_id varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  type varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  name varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  price decimal(8,2) DEFAULT NULL,
  pay decimal(8,2) DEFAULT NULL,
  xtype varchar(2) CHARACTER SET utf8 NOT NULL COMMENT '持仓类型',
  create_time timestamp NULL DEFAULT NULL,
  gain_per int(11) DEFAULT NULL,
  loss_per int(11) DEFAULT NULL,
  trade_no varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  nick_name varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  attr0 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  attr1 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  attr2 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table trade_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS trade_log;

CREATE TABLE trade_log (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  open_id varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  type varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  name varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  price decimal(8,2) DEFAULT NULL,
  yk decimal(8,2) DEFAULT NULL,
  close_price decimal(8,2) DEFAULT NULL,
  pay decimal(8,2) DEFAULT NULL,
  xtype varchar(2) CHARACTER SET utf8 NOT NULL COMMENT '持仓类型',
  create_time timestamp NULL DEFAULT NULL,
  gain_per int(11) DEFAULT NULL,
  loss_per int(11) DEFAULT NULL,
  time timestamp NULL DEFAULT NULL,
  trade_no varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  nick_name varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  attr0 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  attr1 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  attr2 varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS user;

CREATE TABLE user (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  account varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  code varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '可以存微信open_id',
  pwd varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ' ',
  name varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  sex varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  phone varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  we_chat varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  we_chat_name varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  head_pic varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  reg_time timestamp NULL DEFAULT NULL,
  referrer varchar(30) CHARACTER SET utf8 DEFAULT '' COMMENT '推荐人_code',
  login_num bigint(20) DEFAULT NULL,
  user_type varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  pay  DECIMAL(8,2) DEFAULT NULL,
  flag varchar(2) CHARACTER SET utf8 DEFAULT '1',
  attr0 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  attr1 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  attr2 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  attr3 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  attr4 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  attr5 varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  dec0 decimal(10,4) DEFAULT NULL,
  dec1 decimal(10,4) DEFAULT NULL,
  dec2 decimal(10,4) DEFAULT NULL,
  time0 timestamp NULL DEFAULT NULL,
  time1 timestamp NULL DEFAULT NULL,
  time2 timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES user WRITE;
/*!40000 ALTER TABLE user DISABLE KEYS */;