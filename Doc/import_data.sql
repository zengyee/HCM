#import data from gzfk

#用户导入操作
insert into user(account,code,pwd,name,phone,we_chat,we_chat_name,head_pic,login_num,pay)
  select open_id,open_id,'',nick_name,mobile,name,we_chat,img,num,pay from gzfk.user;

update user set we_chat_name = name;

#trade log 交易历史
insert into trade_log(open_id,type,name,price,yk,close_price,pay,xtype,
                      create_time,gain_per,loss_per,trade_no,nick_name)
  select open_id,type,name,price,yk,close_price,pay,xtype,create_time,
    gain_per,loss_per,attr1,attr0 from gzfk.trade_log

#流水
insert into pay_stream(open_id,code,type,pay,before_pay,after_pay,create_time,order_no)
select open_id,'',type,pay,before_pay,after_pay,create_time,attr0 from gzfk.pay_stream;

#支付记录
insert into pay_log(open_id,create_time,pay,status,pay_no)
  select open_id,create_time,pay,status,pay_no from gzfk.pay_log

#提现记录
insert into cash_log(open_id,phone,we_chat,pay,create_time,status,result,flag,message)
  select open_id,phone,we_chat,pay,create_time,status,'',attr1,attr0 from gzfk.cash_log;
