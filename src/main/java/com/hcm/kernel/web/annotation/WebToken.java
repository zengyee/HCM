package com.hcm.kernel.web.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 防止表单重复提交
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WebToken {
    boolean add() default false;  //在需要生成token的controller方法上添加
    boolean valid() default false; //在需要验证token合法性的方法上添加，验证后将从sessoin中删除token，再次提交过来进行校验时将发现两者不匹配，不匹配就无法再次提交。
}
