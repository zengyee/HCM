package com.hcm.kernel.web.filter;

import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 请求字符串编码过滤，强制使用utf-8，强制执行编码处理。不可以使用其它类型字串编码
 */
public class HCMEncodingFilter extends CharacterEncodingFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        setEncoding("utf-8");
        setForceEncoding(true);
        super.doFilterInternal(request, response, filterChain);
    }
}
