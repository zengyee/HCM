package com.hcm.kernel.mvc.dao;

import com.hcm.kernel.util.Pagination;

import java.util.List;
import java.util.Map;

/**
 * 所有数据库操作层代码，简写方便
 */
public interface IKernelDao<T> {

    T insert(T t);
    T delete(T t);
    T update(T t);
    T findById(java.io.Serializable id);

    void deleteByProperties(Map<String, Object> params);
    void deleteByWhere(String where);

    List<T> findAll(Pagination page);
    Long countAll();

    List<?> findByHql(String hql, Pagination page, Map<String, Object> params);
    Long countByHql(String hql, Map<String, Object> params);

    List<?> findBySql(String sql, Pagination page, Map<String, Object> params);
    Long countBySql(String sql, Map<String, Object> params);

    /**
     * 执行删除、更新、插入语句
     * @param sql
     * @param type
     * @return
     */
    int execute(String sql,String type);

    List<T> findByEntity(T t, Pagination page);

    long countByEntity(T t);

    List<T> findByEntity(String hql, T t, Pagination page);

    long countByEntity(String hql, T t);

    List<?> findByProperties(Pagination page,Map<String,Object>...properties);

    long countByProperties(Map<String,Object>...properties);

}
