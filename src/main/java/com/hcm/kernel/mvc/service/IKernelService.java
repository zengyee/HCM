package com.hcm.kernel.mvc.service;

import com.hcm.kernel.util.Pagination;

import java.util.List;

/**
 * 所有业务层的基类，可以简化编写，提供公共方法
 */
public interface IKernelService<T> {

    T add(T t);

    T delete(T t);

    T update(T t);

    void deleteByIds(String ids);

    T findById(int id);

    List<T> findAll(Pagination page);

    Long countAll();

    List<T> findByEntity(T t,Pagination page);

}
