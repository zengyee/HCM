package com.hcm.kernel.mvc.service.impl;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.kernel.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * IKernelService的实现类
 */
public class KernelServiceImpl<T> implements IKernelService<T> {
    @Autowired
    IKernelDao<T> kernelDao;

    public List<T> findAll(Pagination page) {
        return kernelDao.findAll(page);
    }

    public Long countAll() {
        return kernelDao.countAll();
    }

    @Override
    public List<T> findByEntity(T t, Pagination page) {
        return kernelDao.findByEntity(t,page);
    }

    @Transactional(readOnly = false)
    public T add(T o) {
        return kernelDao.insert(o);
    }

    @Transactional(readOnly = false)
    public T delete(T o) {
        return kernelDao.delete(o);
    }

    @Transactional(readOnly = false)
    public T update(T o) {
        return kernelDao.update(o);
    }

    @Transactional(readOnly = false)
    public void deleteByIds(String ids) {
        String where = " and id in (" + ids + ")";
        kernelDao.deleteByWhere(where);
    }

    public T findById(int id) {
        return kernelDao.findById(id);
    }

    public T get0(List<T> list){
        if(list!=null && list.size()>0) return list.get(0);
        return null;
    }
}
