package com.hcm.kernel.spring;

import org.springframework.web.servlet.DispatcherServlet;

/**
 * hcm spring请求控制器
 */
public class HCMWebSocketDispatcherServlet extends DispatcherServlet {

    public HCMWebSocketDispatcherServlet(){
        setContextConfigLocation("classpath:conf/spring/spring-webSocket.xml");
    }

}
