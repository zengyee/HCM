package com.hcm.kernel.spring;

import org.springframework.web.servlet.DispatcherServlet;

/**
 * hcm spring请求控制器
 */
public class HCMDispatcherServlet extends DispatcherServlet {

    public HCMDispatcherServlet(){
        setContextConfigLocation("classpath:conf/spring/spring-mvc.xml");
    }

}
