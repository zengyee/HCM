package com.hcm.kernel.sql;

import java.util.HashMap;
import java.util.Map;

/**
 * 将bean中所有非空字段设置转化为sql的where条件，所有条件是与的关系
 * @author zhangzunwei
 */
public class SQLWhere {

    private String where = " ";
    private Map<String,Object> params = new HashMap<>();

    public void put(String key,Object value){
        params.put(key,value);
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = this.where + " And " + where;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
