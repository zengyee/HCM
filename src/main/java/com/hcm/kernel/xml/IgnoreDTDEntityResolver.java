package com.hcm.kernel.xml;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;

/**
 * 添加在读取xml文件时过滤其中的dtd配置
 * @author zhangzunwei
 *
 */
public class IgnoreDTDEntityResolver implements EntityResolver {

	public InputSource resolveEntity(String publicId, String systemId) {
		return new InputSource(new ByteArrayInputStream(
				"<?xml version='1.0' encoding='UTF-8'?>".getBytes()));
	}

}