package com.hcm.kernel.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期操作
 */
public class DateUtil {

    public static String ZH_FORMAT_DATE_TIME="yyyy-MM-dd HH:mm:ss";
    public static String ZH_FORMAT_DATE="yyyy-MM-dd";
    public static String ZH_FORMAT_TIME="HH:mm:ss";

    /**
     * 根据指定格式返回日期
     * @param date
     * @param format
     * @return
     */
    public static String getDate(Date date ,String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    /**
     * {@link DateUtil getDate()}
     * @param timestamp
     * @param format
     * @return
     */
    public static String getDate(Timestamp timestamp,String format){
        return getDate(timestamp, format);
    }

    /**
     * 根据date获取 type 的值
     * @param calendar
     * @param type
     * @return
     */
    public static int get(Calendar calendar,int type){
        return calendar.get(type);
    }

    public static Calendar getCalendar(Date date){
        Calendar calendar = Calendar.getInstance();
        if(date==null)
            calendar.setTime(new Date());
        else calendar.setTime(date);
        return calendar;
    }

    public static Date parse(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

}
