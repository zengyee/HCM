package com.hcm.kernel.util;

/**
 * Created by zhangzunwei on 14/12/22.
 */
public class Pagination {

    int page = 1;
    int pageSize = 10;
    long total;
    int start = 0;
    int pageTotal = 1;
    int end = 0;

    public Pagination(){}

    public Pagination(int page){
        this.page = page;
    }

    public int getStart(){
        return (page - 1) * pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
        this.pageTotal = (int)(total+pageSize-1)/pageSize;
        if(this.pageTotal == 0) this.pageTotal = 1;
    }

    public void setStart(int start) {
        this.start = start;
        this.page = (start+pageSize - 1)/pageSize;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public int getEnd(){
        end = page * pageSize;
        if(end > total){
            end = (int)total;
        }
        return end;
    }

}
