package com.hcm.kernel.util;

import com.hcm.kernel.spring.SpringUtil;
import com.hcm.module.admin.model.Dic;
import com.hcm.module.admin.service.IDicService;

/**
 * 使用jstl简写输出
 */
public class DicUtil {

    public static String find(String type,String code){
        IDicService dicService = SpringUtil.getBean("dicService");
        Dic dic = dicService.findByTypeAndCode(type,code);
        return dic.getName();
    }
}
