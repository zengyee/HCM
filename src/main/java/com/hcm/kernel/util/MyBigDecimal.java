package com.hcm.kernel.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

/**
 * Created by zhangzunwei on 15/5/29.
 */
public class MyBigDecimal extends BigDecimal {
    public MyBigDecimal(char[] in, int offset, int len) {
        super(in, offset, len);
    }

    public MyBigDecimal(char[] in, int offset, int len, MathContext mc) {
        super(in, offset, len, mc);
    }

    public MyBigDecimal(char[] in) {
        super(in);
    }

    public MyBigDecimal(char[] in, MathContext mc) {
        super(in, mc);
    }

    public MyBigDecimal(String val) {
        super(val);
    }

    public MyBigDecimal(String val, MathContext mc) {
        super(val, mc);
    }

    public MyBigDecimal(double val) {
        super(val);
    }

    public MyBigDecimal(double val, MathContext mc) {
        super(val, mc);
    }

    public MyBigDecimal(BigInteger val) {
        super(val);
    }

    public MyBigDecimal(BigInteger val, MathContext mc) {
        super(val, mc);
    }

    public MyBigDecimal(BigInteger unscaledVal, int scale) {
        super(unscaledVal, scale);
    }

    public MyBigDecimal(BigInteger unscaledVal, int scale, MathContext mc) {
        super(unscaledVal, scale, mc);
    }

    public MyBigDecimal(int val) {
        super(val);
    }

    public MyBigDecimal(int val, MathContext mc) {
        super(val, mc);
    }

    public MyBigDecimal(long val) {
        super(val);
    }

    public MyBigDecimal(long val, MathContext mc) {
        super(val, mc);
    }

    @Override
    public MyBigDecimal add(BigDecimal augend) {
        return new MyBigDecimal(super.add(augend).toString());
    }

    @Override
    public MyBigDecimal subtract(BigDecimal subtrahend) {
        return  new MyBigDecimal(super.subtract(subtrahend).toString());
    }

    @Override
    public MyBigDecimal multiply(BigDecimal multiplicand) {
        return  new MyBigDecimal(super.multiply(multiplicand).toString());
    }

    @Override
    public MyBigDecimal divide(BigDecimal divisor, int scale, int roundingMode) {
        return  new MyBigDecimal(super.divide(divisor, scale, roundingMode).toString());
    }

    @Override
    public MyBigDecimal divide(BigDecimal divisor) {
        return  new MyBigDecimal(super.divide(divisor).toString());
    }

    public static MyBigDecimal ZERO = new MyBigDecimal(0);

    @Override
    public MyBigDecimal setScale(int newScale, int roundingMode) {
        return new MyBigDecimal(super.setScale(newScale, roundingMode).toString());
    }

    @Override
    public String toString() {
        String result = super.toString();
        if(result.indexOf(".")!=-1){
            if(result.substring(result.indexOf(".")).length()==2){
                result = result + "0";
            }
        }
        result = result.replace(".00","");
        return result;
    }

}
