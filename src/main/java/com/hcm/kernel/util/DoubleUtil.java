package com.hcm.kernel.util;

import java.text.DecimalFormat;

/**
 * Created by zhangzunwei on 15/5/12.
 */
public class DoubleUtil {

    public static String format(int size,double number){
        StringBuffer formatString = new StringBuffer("0");
        if(size>0){
            formatString.append(".");
        }
        for (int i = 0; i < size; i++) {
            formatString.append("#");
        }
        DecimalFormat df = new DecimalFormat(formatString.toString());
        return df.format(number);
    }

}
