package com.hcm.kernel.util;

import java.util.Properties;

/**
 * Created by zhangzunwei on 15/5/9.
 */
public class SystemConfig {

    public static Properties properties = System.getProperties();

    static{
        try {
            properties.load(SystemConfig.class.getClassLoader().getResourceAsStream("/conf/system.properties"));
        } catch (Exception e) {
        }
    }

    public static String getString(String key){
        return properties.getProperty(key);
    }

    public static boolean getBoolean(String key){
        String value = properties.getProperty(key,"true");
        return Boolean.parseBoolean(value);
    }

    public static int getInt(String key){
        String value = properties.getProperty(key,"0");
        return Integer.parseInt(value);
    }

}
