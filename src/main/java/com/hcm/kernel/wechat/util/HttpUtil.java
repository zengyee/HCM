package com.hcm.kernel.wechat.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * 使用httpclient执行url，并获取结果
 */
public class HttpUtil {

    private static Logger log = LoggerFactory.getLogger(HttpUtil.class);

    public static String execute(String url,HttpEntity entity){
        log.info("HttpUtil:{}",url);
        HttpPost httpPost = new HttpPost(url);
        return execute(httpPost,entity);
    }

    public static String execute(HttpRequest request,HttpEntity entity){
        CloseableHttpClient httpClient = HttpClients.createDefault();

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

            public String handleResponse(
                    final HttpResponse response) throws IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity, "UTF-8") : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }

        };

        //发送http请求
        String responseBody = null;
        try {
            if(request instanceof HttpPost){
                HttpPost httpPost = (HttpPost)request;
                httpPost.setEntity(entity);
                responseBody = httpClient.execute(httpPost,responseHandler);
            }
            if(request instanceof HttpGet){
                HttpGet httpGet = (HttpGet)request;
                responseBody = httpClient.execute(httpGet,responseHandler);
            }

            log.info("HttpUtil-return:{}",responseBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseBody;
    }

    /**
     * 将list转化为xml文档，父标签为xml
     * @param nvps
     * @return
     */
    public static String toXml(List<NameValuePair> nvps){
        StringBuilder xml = new StringBuilder("<xml>");
        for(NameValuePair nvp:nvps){
            xml.append("<"+nvp.getName()+">");
            xml.append(nvp.getValue());
            xml.append("</"+nvp.getName()+">");
        }
        return xml.append("</xml>").toString();
    }

    /**
     * 将list转化为url地址
     * @param nvps
     * @return
     */
    public static String toUrl(List<NameValuePair> nvps){
        StringBuilder xml = new StringBuilder();
        for(NameValuePair nvp:nvps){
            xml.append(nvp.getName()+"=");
            xml.append(nvp.getValue()+"&");
        }
        return xml.toString();
    }
}
