package com.hcm.kernel.wechat;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.util.HttpUtil;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理与所有微信登陆授权，获取用户信息相关
 */
public class AuthAPI {
    private static Logger log = LoggerFactory.getLogger(AuthAPI.class);

    public static void auth(String appid,String redirectUrl,String scope){
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?response_type=code&scope=%s&state=mt#wechat_redirect";
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("auth");

        url = String.format(url,scope);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("appid",appid));
        try {
            nvps.add(new BasicNameValuePair("redirect_uri", URLEncoder.encode(redirectUrl, "utf-8")));
            HttpUtil.execute(url, new UrlEncodedFormEntity(nvps, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("Auth error:{}",e.getMessage());
        }
    }

    public static String getUserOpenid(String code,String appid,String secret){
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token";
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("getuser");

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("appid",appid));//appid
        nvps.add(new BasicNameValuePair("secret",secret));//密钥
        nvps.add(new BasicNameValuePair("grant_type", "authorization_code"));//随机字符串
        nvps.add(new BasicNameValuePair("code",code));
        try {
            return HttpUtil.execute(url, new UrlEncodedFormEntity(nvps, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("GetUserOpenId error:{}", e.getMessage());
        }
        return "";
    }

    public static String getUserInfo(String openid,String access_token){
        String url = "https://api.weixin.qq.com/sns/userinfo";
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("userinfo");

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("access_token",access_token));
        nvps.add(new BasicNameValuePair("openid",openid));
        nvps.add(new BasicNameValuePair("lang","zh_CN"));
        try {
            log.info(url);
            return HttpUtil.execute(url, new UrlEncodedFormEntity(nvps, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("GetUserInfo error:{}",e.getMessage());
        }
        return "";
    }

}
