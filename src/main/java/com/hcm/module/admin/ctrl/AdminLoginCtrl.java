package com.hcm.module.admin.ctrl;

import com.hcm.kernel.mvc.ctrl.KernelCtrl;
import com.hcm.module.Constants;
import com.hcm.module.admin.ctrl.cache.MenuCache;
import com.hcm.module.admin.model.Role;
import com.hcm.module.admin.model.User;
import com.hcm.module.admin.service.IRoleService;
import com.hcm.module.admin.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 用户登录
 */
@Controller
@RequestMapping("/admin")
public class AdminLoginCtrl extends KernelCtrl {

    @Autowired
    IUserService userService;
    @Autowired
    IRoleService roleService;

    @RequestMapping("/login")
    public String login(User user,ModelMap mm){
        user = userService.login(user);
        if(user.isOutputFlag()){//返回true，说明用户基本信息校验成功
            setUser(user);
            //取用户角色
            List<Role> roleList = roleService.findByUser(user.getId());
            set(Constants.SESSION_ROLES,roleList);

            //取用户菜单
            set(Constants.MAIN_MENU, MenuCache.getMainPageMenu(roleList.get(0).getId()));
            set(Constants.STARTER_MENU, MenuCache.getStarterMenu(roleList.get(0).getId()));

        }else{//登录失败
            mm.put("user",user);
            return "admin/login";
        }
        return "admin/starter";
    }
}
