package com.hcm.module.admin.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.admin.model.User;

public interface IUserDao extends IKernelDao<User> {
}
