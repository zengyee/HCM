package com.hcm.module.admin.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.admin.model.Role;

/**
 * 角色管理
 */
public interface IRoleDao extends IKernelDao<Role> {
}
