package com.hcm.module.admin.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.admin.model.Dic;

/**
 * 数据字典
 */
public interface IDicDao extends IKernelDao<Dic> {
}
