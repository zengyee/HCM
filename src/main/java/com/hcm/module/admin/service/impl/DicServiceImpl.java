package com.hcm.module.admin.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.kernel.util.MapUtil;
import com.hcm.module.admin.dao.IDicDao;
import com.hcm.module.admin.model.Dic;
import com.hcm.module.admin.service.IDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("dicService")
public class DicServiceImpl extends KernelServiceImpl<Dic> implements IDicService {
    @Autowired
    IDicDao dicDao;

    @Override
    public List<Dic> findByType(String type) {
        String hql = "from Dic where type=:type";
        return (List<Dic>) dicDao.findByHql(hql,null, MapUtil.put("type", type));
    }

    @Override
    public Dic findByTypeAndCode(String type, String code) {
        String hql = "from Dic where type=:type and code=:code";
        Map<String,Object> map = MapUtil.put("type",type);
        map.put("code",code);
        List<Dic> dicList = (List<Dic>) dicDao.findByHql(hql,null,map);
        return get0(dicList);
    }
}
