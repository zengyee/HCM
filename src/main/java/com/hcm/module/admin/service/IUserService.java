package com.hcm.module.admin.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.admin.model.User;
import com.hcm.module.gzfk.model.CashLog;

/**
 * 用户管理
 */
public interface IUserService extends IKernelService<User> {

    User login(User user);

    /**
     * 因为在关注时，已经将用添加到user表，所以这里只更新就可以了
     * @param json 用户授权后返回的json串
     * @return
     */
    User update(String json);

    /**
     * 支付成功，并更新后user中，记录流水
     * @param tradeNo
     * @param code
     * @return
     */
    User pay(String tradeNo,String code);

    /**
     * @param code 这里code指open_id
     * @return
     */
    User findByCode(String code);

    /**
     * 平仓，因为买入只可以对一个品种进行一次购买，所以这里没有使用订单号，而使用open_id和type进行操作
     * @param code 平仓人open_id
     * @param type 交易品种
     * @param price 平仓价格，如果为空取最新价格
     * @return
     */
    User close(String code,String type,String...price);

    /**
     * 提现申请
     * @param user
     * @return
     */
    CashLog cash(User user);

}
