package com.hcm.module.admin.service;

import com.hcm.module.admin.model.Dic;

import java.util.List;

/**
 * Created by zhzw on 15/6/26.
 */
public interface IDicService {

    List<Dic> findByType(String type);
    Dic findByTypeAndCode(String type, String code);

}
