package com.hcm.module.admin.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.admin.dao.IUserDao;
import com.hcm.module.admin.model.Role;
import com.hcm.module.admin.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("roleService")
public class RoleServiceImpl extends KernelServiceImpl<Role> implements IRoleService {
    @Autowired
    IUserDao userDao;

    @Override
    public List<Role> findByUser(Integer userId) {
        String hql = "select role from Role role,UserRole ur where ur.roleId = role.id and ur.userId = :userId ";
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        return (List<Role>) userDao.findByHql(hql,null,params);
    }
}
