package com.hcm.module.admin.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 用户
 */
@Entity
public class User extends KernelModel{

    private Integer id;
    private String account;
    private String pwd;
    private String userType;
    private String code;
    private String name;
    private String sex;
    private String phone;
    private String weChat;
    private String weChatName;
    private String headPic;
    private Timestamp regTime;
    private String referrer;
    private Long loginNum;
    private BigDecimal pay;
    private String flag;
    private String attr0;
    private String attr1;
    private String attr2;
    private String attr3;
    private String attr4;
    private String attr5;
    private BigDecimal dec0;
    private BigDecimal dec1;
    private BigDecimal dec2;
    private Timestamp time0;
    private Timestamp time1;
    private Timestamp time2;

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId(){
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "pwd")
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Basic
    @Column(name = "user_type")
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "we_chat")
    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

    @Basic
    @Column(name = "we_chat_name")
    public String getWeChatName() {
        return weChatName;
    }

    public void setWeChatName(String weChatName) {
        this.weChatName = weChatName;
    }

    @Basic
    @Column(name = "head_pic")
    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }

    @Basic
    @Column(name = "reg_time")
    public Timestamp getRegTime() {
        return regTime;
    }

    public void setRegTime(Timestamp regTime) {
        this.regTime = regTime;
    }

    @Basic
    @Column(name = "referrer")
    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    @Basic
    @Column(name = "login_num")
    public Long getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(Long loginNum) {
        this.loginNum = loginNum;
    }

    @Basic
    @Column(name = "pay")
    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    @Basic
    @Column(name = "flag")
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Basic
    @Column(name = "attr0")
    public String getAttr0() {
        return attr0;
    }

    public void setAttr0(String attr0) {
        this.attr0 = attr0;
    }

    @Basic
    @Column(name = "attr1")
    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    @Basic
    @Column(name = "attr2")
    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    @Basic
    @Column(name = "attr3")
    public String getAttr3() {
        return attr3;
    }

    public void setAttr3(String attr3) {
        this.attr3 = attr3;
    }

    @Basic
    @Column(name = "attr4")
    public String getAttr4() {
        return attr4;
    }

    public void setAttr4(String attr4) {
        this.attr4 = attr4;
    }

    @Basic
    @Column(name = "attr5")
    public String getAttr5() {
        return attr5;
    }

    public void setAttr5(String attr5) {
        this.attr5 = attr5;
    }

    @Basic
    @Column(name = "dec0")
    public BigDecimal getDec0() {
        return dec0;
    }

    public void setDec0(BigDecimal dec0) {
        this.dec0 = dec0;
    }

    @Basic
    @Column(name = "dec1")
    public BigDecimal getDec1() {
        return dec1;
    }

    public void setDec1(BigDecimal dec1) {
        this.dec1 = dec1;
    }

    @Basic
    @Column(name = "dec2")
    public BigDecimal getDec2() {
        return dec2;
    }

    public void setDec2(BigDecimal dec2) {
        this.dec2 = dec2;
    }

    @Basic
    @Column(name = "time0")
    public Timestamp getTime0() {
        return time0;
    }

    public void setTime0(Timestamp time0) {
        this.time0 = time0;
    }

    @Basic
    @Column(name = "time1")
    public Timestamp getTime1() {
        return time1;
    }

    public void setTime1(Timestamp time1) {
        this.time1 = time1;
    }

    @Basic
    @Column(name = "time2")
    public Timestamp getTime2() {
        return time2;
    }

    public void setTime2(Timestamp time2) {
        this.time2 = time2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (account != null ? !account.equals(user.account) : user.account != null) return false;
        if (pwd != null ? !pwd.equals(user.pwd) : user.pwd != null) return false;
        if (userType != null ? !userType.equals(user.userType) : user.userType != null) return false;
        if (code != null ? !code.equals(user.code) : user.code != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (sex != null ? !sex.equals(user.sex) : user.sex != null) return false;
        if (phone != null ? !phone.equals(user.phone) : user.phone != null) return false;
        if (weChat != null ? !weChat.equals(user.weChat) : user.weChat != null) return false;
        if (weChatName != null ? !weChatName.equals(user.weChatName) : user.weChatName != null) return false;
        if (headPic != null ? !headPic.equals(user.headPic) : user.headPic != null) return false;
        if (regTime != null ? !regTime.equals(user.regTime) : user.regTime != null) return false;
        if (referrer != null ? !referrer.equals(user.referrer) : user.referrer != null) return false;
        if (loginNum != null ? !loginNum.equals(user.loginNum) : user.loginNum != null) return false;
        if (pay != null ? !pay.equals(user.pay) : user.pay != null) return false;
        if (flag != null ? !flag.equals(user.flag) : user.flag != null) return false;
        if (attr0 != null ? !attr0.equals(user.attr0) : user.attr0 != null) return false;
        if (attr1 != null ? !attr1.equals(user.attr1) : user.attr1 != null) return false;
        if (attr2 != null ? !attr2.equals(user.attr2) : user.attr2 != null) return false;
        if (attr3 != null ? !attr3.equals(user.attr3) : user.attr3 != null) return false;
        if (attr4 != null ? !attr4.equals(user.attr4) : user.attr4 != null) return false;
        if (attr5 != null ? !attr5.equals(user.attr5) : user.attr5 != null) return false;
        if (dec0 != null ? !dec0.equals(user.dec0) : user.dec0 != null) return false;
        if (dec1 != null ? !dec1.equals(user.dec1) : user.dec1 != null) return false;
        if (dec2 != null ? !dec2.equals(user.dec2) : user.dec2 != null) return false;
        if (time0 != null ? !time0.equals(user.time0) : user.time0 != null) return false;
        if (time1 != null ? !time1.equals(user.time1) : user.time1 != null) return false;
        if (time2 != null ? !time2.equals(user.time2) : user.time2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (pwd != null ? pwd.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (weChat != null ? weChat.hashCode() : 0);
        result = 31 * result + (weChatName != null ? weChatName.hashCode() : 0);
        result = 31 * result + (headPic != null ? headPic.hashCode() : 0);
        result = 31 * result + (regTime != null ? regTime.hashCode() : 0);
        result = 31 * result + (referrer != null ? referrer.hashCode() : 0);
        result = 31 * result + (loginNum != null ? loginNum.hashCode() : 0);
        result = 31 * result + (userType != null ? userType.hashCode() : 0);
        result = 31 * result + (pay != null ? pay.hashCode() : 0);
        result = 31 * result + (flag != null ? flag.hashCode() : 0);
        result = 31 * result + (attr0 != null ? attr0.hashCode() : 0);
        result = 31 * result + (attr1 != null ? attr1.hashCode() : 0);
        result = 31 * result + (attr2 != null ? attr2.hashCode() : 0);
        result = 31 * result + (attr3 != null ? attr3.hashCode() : 0);
        result = 31 * result + (attr4 != null ? attr4.hashCode() : 0);
        result = 31 * result + (attr5 != null ? attr5.hashCode() : 0);
        result = 31 * result + (dec0 != null ? dec0.hashCode() : 0);
        result = 31 * result + (dec1 != null ? dec1.hashCode() : 0);
        result = 31 * result + (dec2 != null ? dec2.hashCode() : 0);
        result = 31 * result + (time0 != null ? time0.hashCode() : 0);
        result = 31 * result + (time1 != null ? time1.hashCode() : 0);
        result = 31 * result + (time2 != null ? time2.hashCode() : 0);
        return result;
    }
}
