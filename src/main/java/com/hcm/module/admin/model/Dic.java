package com.hcm.module.admin.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;

/**
 * 数据字典
 */
@Entity
public class Dic extends KernelModel{
    private Integer id;
    private String type;
    private String code;
    private String name;
    private String remark;
    private String flag;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "flag")
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dic dic = (Dic) o;

        if (id != dic.id) return false;
        if (type != null ? !type.equals(dic.type) : dic.type != null) return false;
        if (code != null ? !code.equals(dic.code) : dic.code != null) return false;
        if (name != null ? !name.equals(dic.name) : dic.name != null) return false;
        if (remark != null ? !remark.equals(dic.remark) : dic.remark != null) return false;
        if (flag != null ? !flag.equals(dic.flag) : dic.flag != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (flag != null ? flag.hashCode() : 0);
        return result;
    }
}
