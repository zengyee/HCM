package com.hcm.module.admin.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;

/**
 *  角色关联菜单
 */
@Entity
@Table(name = "role_menu")
@IdClass(RoleMenuPK.class)
public class RoleMenu extends KernelModel{
    private Integer roleId;
    private Integer menuId;

    @Id
    @Column(name = "role_id")
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Id
    @Column(name = "menu_id")
    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleMenu roleMenu = (RoleMenu) o;

        if (roleId != roleMenu.roleId) return false;
        if (menuId != roleMenu.menuId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleId;
        result = 31 * result + menuId;
        return result;
    }
}
