package com.hcm.module.gzfk.ctrl.util;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.AuthAPI;

/**
 * 封装验证api
 */
public class GAuthAPI {

    public static void auth(){
        AuthAPI.auth(SystemConfig.getString("appid"), SystemConfig.getString("redirect_uri"), "snsapi_userinfo");
    }

    public static String getUserOpenid(String code){
        return AuthAPI.getUserOpenid(code, SystemConfig.getString("appid"),SystemConfig.getString("secret"));
    }

    public static String getUserInfo(String openid,String access_token){
        return AuthAPI.getUserInfo(openid,access_token);
    }
}