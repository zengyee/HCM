package com.hcm.module.gzfk.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 充值历史
 */
@Entity
@Table(name = "pay_log")
public class PayLog extends KernelModel {
    private long id;
    private String openId;
    private Timestamp createTime;
    private BigDecimal pay;
    private String status;
    private String payNo;
    private String attr1;
    private String attr2;
    private String attr0;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "pay")
    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "pay_no")
    public String getPayNo() {
        return payNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    @Basic
    @Column(name = "attr1")
    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    @Basic
    @Column(name = "attr2")
    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    @Basic
    @Column(name = "attr0")
    public String getAttr0() {
        return attr0;
    }

    public void setAttr0(String attr0) {
        this.attr0 = attr0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PayLog payLog = (PayLog) o;

        if (id != payLog.id) return false;
        if (openId != null ? !openId.equals(payLog.openId) : payLog.openId != null) return false;
        if (createTime != null ? !createTime.equals(payLog.createTime) : payLog.createTime != null) return false;
        if (pay != null ? !pay.equals(payLog.pay) : payLog.pay != null) return false;
        if (status != null ? !status.equals(payLog.status) : payLog.status != null) return false;
        if (payNo != null ? !payNo.equals(payLog.payNo) : payLog.payNo != null) return false;
        if (attr1 != null ? !attr1.equals(payLog.attr1) : payLog.attr1 != null) return false;
        if (attr2 != null ? !attr2.equals(payLog.attr2) : payLog.attr2 != null) return false;
        if (attr0 != null ? !attr0.equals(payLog.attr0) : payLog.attr0 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (openId != null ? openId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (pay != null ? pay.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (payNo != null ? payNo.hashCode() : 0);
        result = 31 * result + (attr1 != null ? attr1.hashCode() : 0);
        result = 31 * result + (attr2 != null ? attr2.hashCode() : 0);
        result = 31 * result + (attr0 != null ? attr0.hashCode() : 0);
        return result;
    }
}
