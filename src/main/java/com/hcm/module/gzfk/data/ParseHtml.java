package com.hcm.module.gzfk.data;

import com.hcm.kernel.util.DateUtil;
import com.hcm.kernel.util.JSONUtil;
import com.hcm.kernel.util.MyBigDecimal;
import com.hcm.kernel.web.socket.SystemWebSocketHandler;
import com.hcm.module.gzfk.GType;
import com.hcm.module.gzfk.PriceCache;
import com.hcm.module.gzfk.model.Goods;
import com.hcm.module.gzfk.service.IGoodsService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.socket.TextMessage;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * 解析html，并写入数据表goods
 */
public class ParseHtml {

    public List<Goods> get(String html,IGoodsService goodsService) {
        List<Goods> list = new ArrayList<Goods>();
        Map<String,Goods> map = new HashMap<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Timestamp(System.currentTimeMillis()));

        sina_data(html,list,map,calendar);

        //只有这个时间是开市，其他为闭市
        //if(calendar.get(Calendar.HOUR_OF_DAY)<4 || calendar.get(Calendar.HOUR_OF_DAY)>=7) {
            goodsService.add(list);
            SystemWebSocketHandler.sendMessageToUsers(new TextMessage(JSONUtil.toJSONString(map)));
        //}
        return list;
    }


    private void sina_data(String html,List<Goods> list,Map<String,Goods> map,Calendar calendar) {
        String []htmls = html.split(";");
        for(String h:htmls){
            Goods goods = null;
            String []values = h.split(",");

            if(h.indexOf("AU0") != -1){
                goods = new Goods(GType.gold.name(), "黄金");
                if(Float.parseFloat(values[5])>Float.parseFloat(values[7])){
                    goods.setStatus("color: green;");
                }else{
                    goods.setStatus("color: red;");
                }
                //goods.setStatus(e.child(2).child(0).attr("style"));
                goods.setBuy(new MyBigDecimal(values[7]));
                goods.setSell(new MyBigDecimal(values[8]));
                goods.setTime(new Timestamp(System.currentTimeMillis()));
                goods.setCreateTime(new Timestamp(System.currentTimeMillis()));
            }
            if(h.indexOf("AG0")!=-1){
                goods = new Goods(GType.sliver.name(), "白银");
                if(Float.parseFloat(values[5])>Float.parseFloat(values[7])){
                    goods.setStatus("color: green;");
                }else{
                    goods.setStatus("color: red;");
                }
                //goods.setStatus(e.child(2).child(0).attr("style"));
                goods.setBuy(new MyBigDecimal(values[7]));
                goods.setSell(new MyBigDecimal(values[8]));
                goods.setTime(new Timestamp(System.currentTimeMillis()));
                goods.setCreateTime(new Timestamp(System.currentTimeMillis()));
            }
            if(h.indexOf("FU0")!=-1){
                goods = new Goods(GType.oil.name(), "原油");
                if(Float.parseFloat(values[5])>Float.parseFloat(values[7])){
                    goods.setStatus("color: green;");
                }else{
                    goods.setStatus("color: red;");
                }
                //goods.setStatus(e.child(2).child(0).attr("style"));
                float buy = Float.parseFloat(values[7])/7;
                float sell = Float.parseFloat(values[8])/7;
                goods.setBuy(new MyBigDecimal(buy));
                goods.setSell(new MyBigDecimal(sell));
                goods.setTime(new Timestamp(System.currentTimeMillis()));
                goods.setCreateTime(new Timestamp(System.currentTimeMillis()));
            }
            if(goods == null) continue;

            if(PriceCache.gold_pay_buy == null ||PriceCache.sliver_pay_buy == null
                    ||PriceCache.oil_pay_buy == null || calendar.get(Calendar.HOUR_OF_DAY)==7) {
                if (goods.getType().equals("gold")) {
                    PriceCache.gold_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    PriceCache.gold_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    goods.setMore(PriceCache.gold_pay_buy);
                    goods.setBull(PriceCache.gold_pay_sell);
                } else if (goods.getType().equals("sliver")) {
                    PriceCache.sliver_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(1000)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    PriceCache.sliver_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(1000)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    goods.setMore(PriceCache.sliver_pay_buy);
                    goods.setBull(PriceCache.sliver_pay_sell);
                } else if (goods.getType().equals("oil")) {
                    PriceCache.oil_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    PriceCache.oil_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                    goods.setMore(PriceCache.oil_pay_buy);
                    goods.setBull(PriceCache.oil_pay_sell);
                }
            }else{
                if (goods.getType().equals("gold")) {
                    goods.setMore(PriceCache.gold_pay_buy);
                    goods.setBull(PriceCache.gold_pay_sell);
                } else if (goods.getType().equals("sliver")) {
                    goods.setMore(PriceCache.sliver_pay_buy);
                    goods.setBull(PriceCache.sliver_pay_sell);
                } else if (goods.getType().equals("oil")) {
                    goods.setMore(PriceCache.oil_pay_buy);
                    goods.setBull(PriceCache.oil_pay_sell);
                }
            }
            map.put(goods.getType(),goods);
            list.add(goods);
        }
    }

    private void zs_data(String html,List<Goods> list,Map<String,Goods> map,Calendar calendar){
        Document document = org.jsoup.Jsoup.parse(html);
        Element element = document.getElementById("ec_table_body");
        Elements elements = element.children();
        for (Element e : elements) {
            Element td = e.child(1);
            Goods goods = null;
            if (td.html().contains("原油")) {
                goods = new Goods(GType.oil.name(), "原油");
            }
            if (td.html().equals("中色金")) {
                goods = new Goods(GType.gold.name(), "黄金");
            }
            if (td.html().equals("中色银")) {
                goods = new Goods(GType.sliver.name(), "白银");
            }
            if(goods!=null) {
                goods.setStatus(e.child(2).child(0).attr("style"));
                goods.setBuy(new MyBigDecimal(e.child(2).child(0).html()));
                goods.setSell(new MyBigDecimal(e.child(3).child(0).html()));
                goods.setTime(new Timestamp(DateUtil.parse(e.child(7).html()).getTime()));
                goods.setCreateTime(new Timestamp(System.currentTimeMillis()));
                //只有当保证金为空，或者当前时间为凌晨4点时，调整价格。并且这里的价格不再写入数据库
                if(PriceCache.gold_pay_buy == null ||PriceCache.sliver_pay_buy == null
                        ||PriceCache.oil_pay_buy == null || calendar.get(Calendar.HOUR_OF_DAY)==7) {
                    if (goods.getType().equals("gold")) {
                        PriceCache.gold_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        PriceCache.gold_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        goods.setMore(PriceCache.gold_pay_buy);
                        goods.setBull(PriceCache.gold_pay_sell);
                    } else if (goods.getType().equals("sliver")) {
                        PriceCache.sliver_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(1000)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        PriceCache.sliver_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(1000)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        goods.setMore(PriceCache.sliver_pay_buy);
                        goods.setBull(PriceCache.sliver_pay_sell);
                    } else if (goods.getType().equals("oil")) {
                        PriceCache.oil_pay_buy = new MyBigDecimal(goods.getBuy().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        PriceCache.oil_pay_sell = new MyBigDecimal(goods.getSell().divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP).toString());
                        goods.setMore(PriceCache.oil_pay_buy);
                        goods.setBull(PriceCache.oil_pay_sell);
                    }
                }else{
                    if (goods.getType().equals("gold")) {
                        goods.setMore(PriceCache.gold_pay_buy);
                        goods.setBull(PriceCache.gold_pay_sell);
                    } else if (goods.getType().equals("sliver")) {
                        goods.setMore(PriceCache.sliver_pay_buy);
                        goods.setBull(PriceCache.sliver_pay_sell);
                    } else if (goods.getType().equals("oil")) {
                        goods.setMore(PriceCache.oil_pay_buy);
                        goods.setBull(PriceCache.oil_pay_sell);
                    }
                }
                map.put(goods.getType(),goods);
                list.add(goods);
            }

        }
    }

}
