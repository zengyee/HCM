package com.hcm.module.gzfk.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.gzfk.dao.IPayLogDao;
import com.hcm.module.gzfk.model.PayLog;
import com.hcm.module.gzfk.service.IPayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付记录操作
 */
@Service("payLogService")
public class PayLogService extends KernelServiceImpl<PayLog> implements IPayLogService {

    @Autowired
    IPayLogDao payLogDao;

    @Transactional(readOnly = false)
    public void update(String tradeNo, String result) {
        String sql = "update pay_log set status = '"+result+"' where pay_no = '"+tradeNo+"'";
        payLogDao.execute(sql,"sql");
    }
}
