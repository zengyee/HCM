package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.gzfk.model.PayStream;

/**
 * pay stream
 */
public interface IPayStreamService extends IKernelService<PayStream> {
}
