package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.gzfk.model.Goods;

import java.util.List;
import java.util.Map;

/**
 * goods
 */
public interface IGoodsService extends IKernelService<Goods> {

    void add(List<Goods> goodsList);

    /**
     * 登录后查询最新价格，如果有持仓，将持仓结果也取出来
     * @param code open_id
     * @return
     */
    List<Map<String,Object>> lastNew(String code);

}
