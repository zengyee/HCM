package com.hcm.module.gzfk.service.impl;


import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.admin.dao.IUserDao;
import com.hcm.module.admin.model.User;
import com.hcm.module.gzfk.dao.IPayStreamDao;
import com.hcm.module.gzfk.dao.ITradeDao;
import com.hcm.module.gzfk.model.PayStream;
import com.hcm.module.gzfk.model.Trade;
import com.hcm.module.gzfk.service.ITradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 交易操作
 */
@Service("tradeService")
public class TradeService extends KernelServiceImpl<Trade> implements ITradeService {
    @Autowired
    ITradeDao tradeDao;
    @Autowired
    IUserDao userDao;
    @Autowired
    IPayStreamDao payStreamDao;

    @Transactional(readOnly = false)
    public void trade(Trade trade, User user, PayStream payStream) {
        if(payStream != null){
            payStream.setCode("3");
            payStream.setOrderNo(trade.getTradeNo());
            payStreamDao.insert(payStream);
            userDao.update(user);
        }
        tradeDao.insert(trade);
    }
}
