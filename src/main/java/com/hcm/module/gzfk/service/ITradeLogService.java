package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.gzfk.model.TradeLog;

import java.util.List;

/**
 * trade log
 */
public interface ITradeLogService extends IKernelService<TradeLog> {

    /**
     * 对所有用户交易历史进行汇总
     * @return
     */
    List<?> findCount();

    /**
     * 得到最新交易记录
     * @return
     */
    TradeLog getLast();

    List<?> findLastByCode(String code);

}
