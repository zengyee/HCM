package com.hcm.module.gzfk;


import com.hcm.kernel.util.MyBigDecimal;

/**
 * 价格缓冲
 */
public class PriceCache {

    public static MyBigDecimal gold_pay_buy;
    public static MyBigDecimal sliver_pay_buy;
    public static MyBigDecimal oil_pay_buy;

    public static MyBigDecimal gold_pay_sell;
    public static MyBigDecimal sliver_pay_sell;
    public static MyBigDecimal oil_pay_sell;

}
