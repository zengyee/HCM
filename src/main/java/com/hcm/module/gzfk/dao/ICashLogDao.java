package com.hcm.module.gzfk.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.CashLog;

/**
 * cash log dao 提现
 */
public interface ICashLogDao extends IKernelDao<CashLog> {
}
