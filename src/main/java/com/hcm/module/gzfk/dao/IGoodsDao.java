package com.hcm.module.gzfk.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.Goods;

/**
 * goods dao
 */
public interface IGoodsDao extends IKernelDao<Goods> {
}
