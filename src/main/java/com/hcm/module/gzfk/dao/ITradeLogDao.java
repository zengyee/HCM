package com.hcm.module.gzfk.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.TradeLog;

/**
 * trade log dao
 */
public interface ITradeLogDao extends IKernelDao<TradeLog> {
}
