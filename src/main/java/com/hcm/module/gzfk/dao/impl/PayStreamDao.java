package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.IPayStreamDao;
import com.hcm.module.gzfk.model.PayStream;
import org.springframework.stereotype.Repository;

/**
 * 操作流水 dao
 */
@Repository("payStreamDao")
public class PayStreamDao extends KernelDaoImpl<PayStream> implements IPayStreamDao {
}
