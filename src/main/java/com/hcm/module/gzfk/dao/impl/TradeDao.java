package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.ITradeDao;
import com.hcm.module.gzfk.model.Trade;
import org.springframework.stereotype.Repository;

/**
 * 交易操作 dao
 */
@Repository("tradeDao")
public class TradeDao extends KernelDaoImpl<Trade> implements ITradeDao {
}
