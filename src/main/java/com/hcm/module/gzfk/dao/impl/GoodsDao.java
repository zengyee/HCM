package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.IGoodsDao;
import com.hcm.module.gzfk.model.Goods;
import org.springframework.stereotype.Repository;

/**
 * 品种 dao
 */
@Repository("goodsDao")
public class GoodsDao extends KernelDaoImpl<Goods> implements IGoodsDao {
}
