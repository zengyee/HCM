package com.hcm.module.gzfk.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.PayLog;

/**
 * pay log dao
 */
public interface IPayLogDao extends IKernelDao<PayLog> {
}
