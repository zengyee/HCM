<%--
  平仓
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header yellow_bg">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">
                +平仓
            </h4>
        </div>
        <div class="modal-body">
            <div class="czje">
                <ul>
                    <li><span>{{name}}</span>品种</li>
                    <li class="chek"><span>空</span>方向</li>
                    <li><span>1手</span>手数</li>
                </ul>
                <ul>
                    <li><span>{{price}}</span>建仓价(元/克)</li>
                    <li><span>{{pay}}</span>保证金(元)</li>
                    <li><span class="red">{{buy}}</span>平仓价(元/克)</li>
                </ul>
                <ul>
                    <li><span>{{yk}}</span>盈亏(元)</li>
                    <li><span>{{yk_per}}%</span>盈利率</li>
                    <li class="pcbtn"><input type="image" onclick="submit();"
                             name="button" class="czbtn" src="/wx/images/pcbtn.jpg"></li>
                </ul>
            </div>

        </div>

    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal -->
<script>
    var numx = 0;
    function submit(){
        if(numx == 0){
            numx ++;
            disable_button();
            window.location='close.htm?type={{type}}';
        }else{
            alert("等待页面处理中，请不要重复操作好不好。");
        }
    }
</script>