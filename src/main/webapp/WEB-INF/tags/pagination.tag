<%@ tag pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="page" type="com.hcm.kernel.util.Pagination" required="true" %>
<%@ attribute name="url" type="java.lang.String" required="true" %>
<%@ attribute name="params" type="java.lang.String" required="false" %>
<div>
  <c:if test="${page.total == 0}">暂无记录</c:if>
  <c:if test="${page.total > 0}">
    当前显示 ${page.start+1} 到 ${page.end} 条，共${page.pageTotal}页/${page.total}条记录
    <ul id="visible-pages-example" style="margin:0;float: right;"></ul>
    <script>
      $(document).ready(function () {
        $('#visible-pages-example').twbsPagination({
          totalPages: ${page.pageTotal},
          startPage:${page.page},
          first: '首页',
          prev: '前页',
          next: '后页',
          last: '未页',
          href:'javascript:ajaxLoad("${url}?page={{number}}${params}");'
        });
      });
    </script>
  </c:if>
</div>