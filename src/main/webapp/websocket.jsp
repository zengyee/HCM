<%--
  Created by IntelliJ IDEA.
  User: zhzw
  Date: 15/6/18
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
  <script src="./static/jquery/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="static/socket/socket.js"></script>
          
  <script>
    var websocket;
    if ('WebSocket' in window) {
      websocket = new WebSocket("ws://192.168.1.100:8080/webSocket/webSocketServer");
    } else if ('MozWebSocket' in window) {
      websocket = new MozWebSocket("ws://localhost:8080/hcm/webSocket/webSocketServer");
    } else {
      websocket = new SockJS("http://localhost:8080/hcm/webSocket/sockjs/webSocketServer");
    }
    websocket.onopen = function (evnt) {
    };
    websocket.onmessage = function (evnt) {
      $("#msgcount").append("(<span color='red'>" + evnt.data + "</span>)");
    };
    websocket.onerror = function (evnt) {
    };
    websocket.onclose = function (evnt) {
    }

  </script>
</head>
<body>
<div id="msgcount"></div>
</body>
</html>