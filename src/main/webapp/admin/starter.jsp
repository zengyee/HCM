<%--
  框架主页面，所有的内容将通过ajax加载。针对非IE浏览器作了相关优化，可以实现回退和前进
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
  <title>管理系统-首页</title>
  <%@include file="../common/init.jsp"%>
  <link href="static/custom/starter/css.css" type="text/css" rel="stylesheet">
  <script src="static/custom/starter/starter.js"></script>
</head>
<body>
<div class="charm right-side padding20 bg-grayDark" id="charmSettings">
  <button class="square-button bg-transparent fg-white no-border place-right small-button" onclick="showSettings()"><span class="mif-cross"></span></button>
  <h1 class="text-light">背景色设置</h1>
  <hr class="thin"/>
  <br />
  <div class="schemeButtons">
    <div class="button square-button tile-area-scheme-dark" data-scheme="dark"></div>
    <div class="button square-button tile-area-scheme-darkBrown" data-scheme="darkBrown"></div>
    <div class="button square-button tile-area-scheme-darkCrimson" data-scheme="darkCrimson"></div>
    <div class="button square-button tile-area-scheme-darkViolet" data-scheme="darkViolet"></div>
    <div class="button square-button tile-area-scheme-darkMagenta" data-scheme="darkMagenta"></div>
    <div class="button square-button tile-area-scheme-darkCyan" data-scheme="darkCyan"></div>
    <div class="button square-button tile-area-scheme-darkCobalt" data-scheme="darkCobalt"></div>
    <div class="button square-button tile-area-scheme-darkTeal" data-scheme="darkTeal"></div>
    <div class="button square-button tile-area-scheme-darkEmerald" data-scheme="darkEmerald"></div>
    <div class="button square-button tile-area-scheme-darkGreen" data-scheme="darkGreen"></div>
    <div class="button square-button tile-area-scheme-darkOrange" data-scheme="darkOrange"></div>
    <div class="button square-button tile-area-scheme-darkRed" data-scheme="darkRed"></div>
    <div class="button square-button tile-area-scheme-darkPink" data-scheme="darkPink"></div>
    <div class="button square-button tile-area-scheme-darkIndigo" data-scheme="darkIndigo"></div>
    <div class="button square-button tile-area-scheme-darkBlue" data-scheme="darkBlue"></div>
    <div class="button square-button tile-area-scheme-lightBlue" data-scheme="lightBlue"></div>
    <div class="button square-button tile-area-scheme-lightTeal" data-scheme="lightTeal"></div>
    <div class="button square-button tile-area-scheme-lightOlive" data-scheme="lightOlive"></div>
    <div class="button square-button tile-area-scheme-lightOrange" data-scheme="lightOrange"></div>
    <div class="button square-button tile-area-scheme-lightPink" data-scheme="lightPink"></div>
    <div class="button square-button tile-area-scheme-grayed" data-scheme="grayed"></div>
  </div>
</div>

<div class="tile-area tile-area-scheme-darkBlue fg-white">
  <h1 class="tile-area-title">系统管理首页</h1>
  <div class="tile-area-controls">
    <button class="image-button icon-right bg-transparent fg-white bg-hover-dark no-border">
      <span class="sub-header no-margin text-light">欢迎你，系统管理员</span> <span class="icon mif-user"></span></button>
    <button class="square-button bg-transparent fg-white bg-hover-dark no-border" onclick="showSettings()"><span class="mif-cog"></span></button>
    <a href="admin/login.jsp" class="square-button bg-transparent fg-white bg-hover-dark no-border"><span class="mif-switch"></span></a>
  </div>

    ${starter_menu}

</div>
</body>
</html>