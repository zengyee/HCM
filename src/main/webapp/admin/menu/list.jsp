<%@ page contentType="text/html;charset=UTF-8" %>
<%@include file="../init.jsp" %>
<div class="row">
    <div class="col-xd-12">
        <h4 class="page-header">
            <ul class="breadcrumbs2 small">
                <li><a href="javascript:void(0);"><span class="icon mif-home"></span></a></li>
                <li><a href="#">系统管理</a></li>
                <li><a href="#">菜单列表</a></li>
            </ul>
            <div style="float:right;">
                <button class="button primary"
                        onclick="$('#dialog').load('admin/menu/add.jsp').data('dialog').open();">
                    添加
                </button>
                <button class="button danger">删除</button>
                <button class="button info">修改</button>
                <button class="button success">查询</button>
            </div>
        </h4>
    </div>
</div>
<div class="row">
    <div class="col-xd-12">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th style="width:2%"><input type="checkbox" value="-2" onclick=""></th>
                <th>序号</th>
                <th>菜单名</th>
                <th>图标</th>
                <th>图标2</th>
                <th>地址</th>
                <th>父序号</th>
                <th>有效？</th>
                <th>背景色</th>
                <th>排序</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${list}" var="menu">
                <tr>
                    <td><input type="checkbox" value="${area.id}"></td>
                    <td>${menu.id}</td>
                    <td>${menu.name}</td>
                    <td>${menu.icon}</td>
                    <td>${menu.icon2}</td>
                    <td>${menu.url}</td>
                    <td>${menu.pId}</td>
                    <td>${fn_hcm:dicName("flag",menu.flag)}</td>
                    <td>${menu.bgcolor}</td>
                    <td>${menu.display}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <hcm:pagination page="${page}" url="admin/menu/list.htm"/>
    </div>
</div>
<div data-role="dialog" id="dialog" class="padding20" data-close-button="true" data-overlay="true" data-overlay-color="op-dark"></div>