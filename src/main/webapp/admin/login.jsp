<%@page pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>管理系统-登录页</title>
    <%@include file="../common/init.jsp" %>
</head>
<body class="bg-darkBlue">
<div class="container-fruit">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h2 class="login-head"></h2>
            <form method="post" action="admin/login.htm">
                <div class="panel panel-primary">
                    <div class="panel-heading">系统管理后台&copy;</div>
                    <div class="panel-body">
                        <div class="grid">
                            <div class="row">
                                <div class="cell">
                                <span style="color:red;">${user.outputInfo}</span></div>
                            </div>
                            <div class="row">
                                <div class="cell">
                                    <label>用户名</label>
                                    <div class="input-control text full-size">
                                        <input type="text" name="account" placeholder="请输入用户名">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell">
                                    <label>密码</label>

                                    <div class="input-control text full-size">
                                        <input type="password" name="pwd" placeholder="请输入密码">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell">
                                    <label class="input-control checkbox">
                                        <input type="checkbox" checked>
                                        <span class="check"></span>
                                        <span class="caption">记住用户名</span>
                                    </label>
                                    <label class="input-control get-pass">
                                        <a href=""> 找回密码？</a>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell">
                                    <div class="input-control text full-size">
                                        <input type="submit" value="登录"
                                               class="button info block-shadow-info text-shadow">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="footer">
    <span>请不要使用IE8以上浏览器。</span>可以使用IE9+、火狐（FireFox）、谷歌（Chrome）浏览器。或者360浏览器、QQ浏览器的极速模式。
</div>
</body>
</html>