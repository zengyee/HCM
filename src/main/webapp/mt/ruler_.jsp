<%--
  Created by IntelliJ IDEA.
  User: zhangzunwei
  Date: 15/5/28
  Time: 09:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>1863微交易-交易规则</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
</head>
<body>
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title" id="">
        什么是微交易
      </h4>
    </div>
    <div class="modal-body dialog-caozuo" id="despoit_sel">
      <div>
        &nbsp; &nbsp; 为了投资者进一步体验实盘交易的规则，熟悉交易的收益与风险，微交易让用户有机会利用微小额资金，进行等比缩小的交易。微交易的不仅缩小交易额度，还进行交易频率与止盈/
        <span style="background-color:#FFFFFF;">止损的规则限制（在实盘中无限制），帮助投资者养成良好的投资习惯。</span>
      </div>
      <div>
        <span style="line-height:1.5;">微交易与实盘的成交单位表</span>
      </div>
      <div>
        <table border="1" cellpadding="2" cellspacing="0" style="width: auto;">
          <tbody>
          <tr>
            <td valign="top" style="width: 129px;"><b>品种\平台</b></td>
            <td valign="top" style="width: 129px;">
              <div>
                <b>微交易</b>
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                <b>实盘</b>
              </div>
            </td>
            <td valign="top" style="width: 129px;"><b>*相差倍数</b></td>
          </tr>
          <tr>
            <td valign="top" style="width: 129px;">
              <div>
                <b>黄金</b>
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                1 克
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                1 千克
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                x 1000
              </div>
            </td>
          </tr>
          <tr>
            <td valign="top" style="width: 129px;"><b>白银</b></td>
            <td valign="top" style="width: 129px;">
              <div>
                0.1 千克
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                30 千克
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                x 300
              </div>
            </td>
          </tr>
          <tr>
            <td valign="top" style="width: 129px;">
              <div>
                <b>原油</b>
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                1桶
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                500桶
              </div>
            </td>
            <td valign="top" style="width: 129px;">
              <div>
                x 500
              </div>
            </td>
          </tr>
          </tbody>
        </table>
      </div>

      <div>
        <p><span>微交易的交易规则以及注意事项</span></p>
      </div>
      <div>
        <ol>
          <li><span style="line-height: 1.5;">微交易仅限于投资客户体验使用，系非营利性体验</span><span style="line-height: 1.5;">类软件。</span>
          </li>
          <li><span style="line-height: 1.5;">微交易的体验途径仅限制客户通过自己的微信关注本公司的微信服务号后，点击微交易链接，进行体验。</span></li>
          <li><span style="line-height: 1.5;">体验产品有：黄金、原油、白银。</span></li>
          <li><span style="line-height: 1.5;">微交易的体验产品对应的成交单位分别是：黄金<span lang="EN-US">=1克</span>、原油<span
                  lang="EN-US">=1</span>桶、白银<span lang="EN-US">=0.1千克，与实盘交易单位不同</span>。</span></li>
          <li>微交易暂时不收取任何交易手续费。</li>
          <li><span style="line-height: 1.5;">微交易提现时，提现金额仅可全额提现。</span></li>
          <li><span style="line-height: 1.5;">提现到账时间为<span lang="EN-US">T+1</span>到账。</span></li>
          <li><span style="line-height: 1.5;">微交易每天的开、闭市时间为周一到周五早<span lang="EN-US">7</span>点至次日<span
                  lang="EN-US">4</span>点。</span></li>
          <li><span style="line-height: 1.5;">微交易体验限制：</span></li>
          <ol>
            <li><span style="line-height: 1.5;">单次单个品种仅能交易</span><span lang="EN-US"
                                                                       style="line-height: 1.5;">1</span><span
                    style="line-height: 1.5;">手。</span></li>
            <li><span style="line-height: 1.5;">每个账号每天仅能体验</span><span lang="EN-US"
                                                                       style="line-height: 1.5;">10</span><span
                    style="line-height: 1.5;">次（建仓+</span><span style="line-height: 1.5;">平仓视为</span><span lang="EN-US"
                                                                                                           style="line-height: 1.5;">1</span><span
                    style="line-height: 1.5;">次）。</span></li>
            <li><span style="line-height: 1.5;">每个账号体验周期为</span><span lang="EN-US"
                                                                      style="line-height: 1.5;">7</span><span
                    style="line-height: 1.5;">个自然日（包含节假日），到期后用户禁止在微交易中交易。</span></li>
          </ol>
          <li><span style="line-height: 1.5;">一个手机号与微信号只能注册一个账号进行体验操作。</span></li>
          <li><span style="line-height: 1.5;">当账户余额大于<span lang="EN-US">20</span>元时，限制充值。</span></li>
          <li><span style="line-height: 1.5;">每笔微交易 </span><span
                  style="line-height: 1.5;">当日必须平仓，如不手动平仓，到达闭市时间，系统将自动平仓处理。</span></li>
          <li><span style="line-height: 1.5;">在产生订单时，会有二次确认信息，点击确认后订单立即生成，不可撤销！</span></li>
          <li><span style="line-height: 1.5;">止盈/止损规则（体验微交易特殊规则）：</span></li>
          <ol>
            <li><span style="line-height: 1.5;">建仓时，系统自动默认设置平仓线，用户可自在范围内调整。</span></li>
            <li><span style="line-height: 1.5;">默认止损线为：体验品种的保证金数额的</span><span lang="EN-US" style="line-height: 1.5;">100%亏损</span><span
                    style="line-height: 1.5;">。</span></li>
            <li><span style="line-height: 1.5;">默认止盈线为：体验品种的保证金的</span><span lang="EN-US"
                                                                             style="line-height: 1.5;">100%</span><span
                    style="line-height: 1.5;">。</span></li>
            <li><span style="line-height: 1.5;">用户可在建仓确认时，自行在2</span><span lang="EN-US" style="line-height: 1.5;">0%-100%</span><span
                    style="line-height: 1.5;">之间调整止损/止盈的比例。</span></li>
          </ol>
          <li><span style="line-height: 1.5;">体验客户产生的实际盈亏自行承担。解释权归高欧得（北京）信息科技有限公司郑州分公司所有。</span></li>
        </ol>
      </div>
    </div></div></div>
</body>
</html>
