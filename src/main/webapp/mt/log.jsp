<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty log}">
<tr>
  <td>${log.attr0}</td>
  <td>${log.attr1}</td>
  <td>${log.xtype}</td>
  <td style="text-align: right;">
    <c:if test="${log.yk > 0}"><span style="color:red;">${log.yk}</span></c:if>
    <c:if test="${log.yk == 0}"><span>${log.yk}</span></c:if>
    <c:if test="${log.yk < 0}"><span style="color:green;">${log.yk}</span></c:if>
  </td>
</tr>
</c:if>