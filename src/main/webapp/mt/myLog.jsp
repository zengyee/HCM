<%--
  Created by IntelliJ IDEA.
  User: zhangzunwei
  Date: 15/5/28
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>1863微交易-我的交易历史</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
  <%@include file="init.jsp" %>
</head>
<body>
<table class="table table-striped">
  <tr class="index_table_header">
    <td>用户</td>
    <td>品种</td>
    <td>方向</td>
    <td>盈亏</td>
  </tr>
  <tbody id="logBody">
  <c:forEach items="${list}" var="log">
    <tr>
      <td>${log.attr0}</td>
      <td>${log.name}</td>
      <td>${log.xtype}</td>
      <td style="text-align: right;">
        <c:if test="${log.yk > 0}"><span style="color:red;">${log.yk}</span></c:if>
        <c:if test="${log.yk == 0}"><span>${log.yk}</span></c:if>
        <c:if test="${log.yk < 0}"><span style="color:green;">${log.yk}</span></c:if>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
</body>
</html>
