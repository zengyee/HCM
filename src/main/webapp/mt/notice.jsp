<%--
  Created by IntelliJ IDEA.
  User: zhangzunwei
  Date: 15/5/28
  Time: 09:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>1863微交易-开户须知</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
</head>
<body>
<div>
    <div>为了保障您的权益，交易所要求实名认证开户，阅读并熟悉交易的规则与风险，并要通过银行进行出入金的授权绑定。但我们同时为您提供简易的在线开户方法，您需要确保与准备好：</div>
    <div>1、在 正常交易日的早8：00-晚10：00提交；</div>
    <div>2、本人手机号（可注册接收验证短信）；</div>
    <div>3、本人建设银行卡（开通网银）。</div>
    <br>

    <div>步骤：</div>
    <div>1、打开 <a href="http://zs.zhongse.com:16921/SelfOpenAccount/first.jsp?memNo=139">中色金属贸易中心</a>
    </div>
    <div>2、填写基本信息</div>
    <div>3、 阅读并同意风险提示书</div>
    <div>4、完善详细信息</div>
    <div>5、生成实盘账号</div>
    <div>6、下载并安装客户交易端</div>
    <div>7、建设银行网银签约</div>
    <div>8、入金交易</div>
    <br>
    <div>其他注意事项：</div>
    <div>1、开户时身份证必须为二代身份证，银行卡必须是二代身份证办理的，并且姓名要一致。</div>
    <div>2、实盘账号自动激活额度：入金一万，低于该金额激活请联系客服。</div>
    <div>3、签约以及出入金时间为：签约 8:00~20:00，入金 8:00~22:00，出金 9:00~17:00 （均为工作日）。</div>
</div>
</body>
</html>
