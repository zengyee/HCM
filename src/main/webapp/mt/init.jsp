<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<base href="http://godweipan.ijueke.com/mt/"/>
<link href="../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="/main.css" rel="stylesheet" type="text/css">
<script src="../resources/jquery/jquery-1.11.1.min.js"></script>
<script src="../resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../resources/angular/angular.min.js"></script>
<script src="../resources/angular/angular-route.min.js"></script>